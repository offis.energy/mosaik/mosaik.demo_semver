# -*- coding: utf-8 -*-

import datetime

from setuptools import setup, find_packages

TIMESTAMP = str(datetime.datetime.now().replace(microsecond=0).isoformat()).\
    replace('-', '').replace('T', '').replace(':', '')


setup(
    name='mosaik.Demo_SemVer',
    version='0.1.0' + 'rc' + TIMESTAMP,
    classifiers=[
        'License :: OSI Approved :: GNU Lesser General Public License v2 '
        '(LGPLv2)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: PyPy',
    ],
    description='Demonstration project for the mosaik co-simulation middleware.',
    url='https://gitlab.com/offis.energy/mosaik/mosaik.demo_semver',
    author='Bengt Lüers',
    author_email='bengt.lueers@gmail.com',
    long_description=(
        open('README.md').read()
    ),
    long_description_content_type='text/markdown',
    maintainer='Bengt Lüers',
    maintainer_email='bengt.lueers@gmail.com',
    packages=find_packages(exclude=["test", "tests"]),
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'mosaik-demo-hour=mosaik_demo.demo.demo:run_for_one_hour',
            'mosaik-demo-day=mosaik_demo.demo.demo:run_for_one_day',
            'mosaik-demo-month=mosaik_demo.demo.demo:run_for_one_month',
            'mosaik-demo-forever=mosaik_demo.demo.demo:run_forever',
        ],
    },
    install_requires=[
        'mosaik.API-SemVer>=2.4.2rc20190716091443',
        'mosaik.Core-SemVer>=2.5.2rc20190715231038',
        'mosaik-pypower>=0.7.2',
    ],
    setup_requires=[
    ],
)
